#!/usr/bin/env python
import rospy
import numpy as np
from PIL import Image
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry, OccupancyGrid
from math import sqrt, cos, sin, pi, atan2, exp, atan, acos, asin,ceil, floor
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from time import sleep
from visualization_msgs.msg import Marker, MarkerArray
from sensor_msgs.msg import LaserScan
import tf
import sys

try:
    from ompl import base as ob
    from ompl import geometric as og
except ImportError:
    # if the ompl module is not in the PYTHONPATH assume it is installed in a
    # subdirectory of the parent directory called "py-bindings."
    from os.path import abspath, dirname, join
    import sys

    sys.path.insert(0, join(dirname(dirname(abspath(__file__))), 'py-bindings'))
    from ompl import base as ob
    from ompl import geometric as og


# Frequencia de simulacao no stage
global freq
freq = 20.0  # Hz

# Estados do robo
global x_n, y_n, theta_n
x_n = 0.1  # posicao x atual do robo
y_n = 0.2  # posicao y atual do robo
theta_n = 0.001  # orientacao atual do robo

# Posicao objetivo
global x_goal, y_goal
x_goal = 0
y_goal = 0


# Posicao objetivo
global x_pf, y_pf
x_pf = 0
y_pf = 0

#scan
global samples, scan_ang_rad, d_obs, x_Vd_obs, y_Vd_obs
x_Vd_obs = 0
y_Vd_obs = 0
d_obs = 0.001
samples = 270

#Parametros da forca de atracao e repulcao
global d_o, eta, xi
d_o = 4.8
eta =100   #repulsao
xi = 0.2   #atracao

# Velocidade de saturacao
global Usat, Umin
Usat = 5
Umin = 0

# Deslocamento do centro para Feedback Linearization
global d
d = 0.40

global Kp1, Kp2
Kp1 = 1.5
Kp2 = 1.5
global x_0, y_0

# Variaveis globais do supervisor
global tarefas, flag_tarefas, nt, er, e, ep, cont_vel, velo, p, s, x_n_ant, y_n_ant, cont_x, gr, od, no, fp
nt = 0
flag_tarefas = 0
er = 0
e = 1
ep = 0
cont_vel = 0
velo = 0
p = 1
s = 0
x_n_ant = 0
y_n_ant = 0
cont_x = 0
gr = 0
od = 0
no = 1
fp = 1
tarefas = -200 * np.ones(4)
# ----------  ----------  ----------  ----------  ----------

global nCell,mapData

nCell = 100
mapData = np.ones((100, 100))

# Rotina callback para a obtencao da pose do robo
def callback_pose(data):
    global x_n, y_n, theta_n, x_n_ant, y_n_ant, cont_x

    x_n = data.pose.pose.position.x  # posicao 'x' do robo no mundo
    y_n = data.pose.pose.position.y  # posicao 'y' do robo no mundo
    x_q = data.pose.pose.orientation.x
    y_q = data.pose.pose.orientation.y
    z_q = data.pose.pose.orientation.z
    w_q = data.pose.pose.orientation.w
    euler = euler_from_quaternion([x_q, y_q, z_q, w_q])
    theta_n = euler[2]  # orientacao 'theta' do robo no mundo
    if cont_x == 0:
        x_n_ant = x_n
        y_n_ant = y_n
    if cont_x == 20:
        if abs(x_n_ant - x_n) < 0.01 and abs(y_n_ant - y_n) < 0.01:
            s = 1
        else:
            s = 0

    cont_x += 1
    #print('position robot')
    #print(x_n)
    #print(y_n)

    # Atualiza uma transformacao rigida entre os sistemas de coordenadas do mundo e do robo
    # Necessario para o rviz apenas
    br = tf.TransformBroadcaster()
    br.sendTransform((x_n, y_n, 0), (x_q, y_q, z_q, w_q), rospy.Time.now(), "/robot_0/base_pose_ground_truth", "world")

    return


# ----------  ----------  ----------  ----------  ----------

# Rotina callback para a obtencao da informacao do laser
def callback_laser(scan):
    global theta_n, samples, d_obs, x_Vd_obs, y_Vd_obs

    # Procura menor distancia e armazena a posicao encontrada
    smaller = scan.ranges[0]
    ind = 0
    for i in range(0, samples):
        if scan.ranges[i] < smaller:
            smaller = scan.ranges[i]
            ind = i

    # Menor distancia
    d_obs = smaller
    # Angulo do feixe que indica menor distancia [graus]
    d_obs_ang = ind
    # Angulo do sensor no referencial do robo [rad]
    d_obs_robot = (d_obs_ang - 180)*pi/180

    # Gradiente no referencial do robo (Sentido contrario ao menor feixe)
    x_Vd_Robs = -cos(d_obs_robot)
    y_Vd_Robs = -sin(d_obs_robot)

    # Gradiente no referencial inercial
    x_Vd_obs = cos(theta_n) * x_Vd_Robs - sin(theta_n) * y_Vd_Robs
    y_Vd_obs = sin(theta_n) * x_Vd_Robs + cos(theta_n) * y_Vd_Robs

    return

# ----------  ----------  ----------  ----------  ----------

# Rotina callback para a obtencao da informacao do laser
def callback_pf(data):
    global x_pf, y_pf

    x_pf = data.pose.pose.position.x
    y_pf = data.pose.pose.position.y

    return

# ----------  ----------  ----------  ----------  ----------

# Rotina callback para a obtencao da pose do robo que representa o goal
def callback_goal(data):
    global x_goal, y_goal, nt, tarefas, flag_tarefas, x_n, y_n, gr, x_0, y_0
    if flag_tarefas < 2:
        if flag_tarefas == 0:
            tarefas[0] = data.pose.pose.position.x
            tarefas[1] = data.pose.pose.position.y
            x_0 = x_n
            y_0 = y_n
            flag_tarefas += 1
            nt = 1
        elif tarefas[0] != data.pose.pose.position.x and tarefas[1] != data.pose.pose.position.y:
            tarefas[2] = data.pose.pose.position.x
            tarefas[3] = data.pose.pose.position.y
            flag_tarefas += 1
            nt = 1
    if euc_dist(x_n,y_n,tarefas[0],tarefas[1]) <= 0.5:
        flag_tarefas -= 1
        if flag_tarefas == 1:
            x_0 = tarefas[0]
            y_0 = tarefas[1]
            tarefas[0] = tarefas[2]
            tarefas[1] = tarefas[3]

    return

# Rotina callback para a obtencao da pose do robo que representa o goal
def callback_communicate(goal):

    return

# Rotina callback para a obtencao da pose do robo que representa o goal
def callback_traject(goal):

    return

# ----------  ----------  ----------  ----------  ----------

# Rotina para calcular distancia euclidiana
def euc_dist(x1, y1, x2, y2):
    return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2))
# ----------  ----------  ----------  ----------  ----------

# Rotina para forca de atracao
def attration(x, y):
    global xi, x_n, y_n

    fatt_x = -xi*(x_n - x)
    fatt_y = -xi*(y_n - y)

    return fatt_x, fatt_y


# ----------  ----------  ----------  ----------  ----------

# Rotina para forca de repulsion
def repulsion():
    global eta, d_o
    global x_n, y_n, x_goal,y_goal
    global d_obs, x_Vd_obs, y_Vd_obs

    if d_obs > d_o:
        frep_x = 0
        frep_y = 0
    else:
        frep_x = eta*((1/d_obs)-(1/d_o))*(1/(d_obs*d_obs))*x_Vd_obs
        frep_y = eta*((1/d_obs)-(1/d_o))*(1/(d_obs*d_obs))*y_Vd_obs


    return frep_x, frep_y

# ----------  ----------  ----------  ----------  ----------

def trajectory_controller(x, y, Vx_goal, Vy_goal):
    global x_n, y_n
    global Kp1, Kp2

    Ux = Vx_goal + Kp1*(x - x_n)
    Uy = Vy_goal + Kp2*(y - y_n)

    return Ux, Uy

# ----------  ----------  ----------  ----------  ----------

# Rotina feedback linearization
def feedback_linearization(Ux, Uy):
    global x_n, y_n, theta_n
    global d

    VX = cos(theta_n)*Ux + sin(theta_n)*Uy
    WZ = ((-sin(theta_n))/d)*Ux + ((cos(theta_n))/d)*Uy

    return VX, WZ


# ----------  ----------  ----------  ----------  ----------

# Rotina para piblicar informacoes no rviz
def send_marker_to_rviz(x_ref, y_ref, Ux, Uy):
    global x_n, y_n, theta_n
    global x_goal, y_goal
    global pub_rviz_ref, pub_rviz_pose

    # Inicializa mensagens do tipo Marker
    mark_ref = Marker()
    mark_vel = Marker()

    # Define um marcador para representar o alvo
    mark_ref.header.frame_id = "/world"
    mark_ref.header.stamp = rospy.Time.now()
    mark_ref.id = 0
    mark_ref.type = mark_ref.SPHERE
    mark_ref.action = mark_ref.ADD
    mark_ref.scale.x = 0.5
    mark_ref.scale.y = 0.5
    mark_ref.scale.z = 0.5
    mark_ref.color.a = 1.0
    mark_ref.color.r = 1.0
    mark_ref.color.g = 0.0
    mark_ref.color.b = 0.0
    mark_ref.pose.position.x = x_ref
    mark_ref.pose.position.y = y_ref
    mark_ref.pose.position.z = 0.25
    mark_ref.pose.orientation.x = 0
    mark_ref.pose.orientation.y = 0
    mark_ref.pose.orientation.z = 0
    mark_ref.pose.orientation.w = 1

    # Define um marcador para representar a "velocidade desejada"
    mark_vel.header.frame_id = "/world"
    mark_vel.header.stamp = rospy.Time.now()
    mark_vel.id = 1
    mark_vel.type = mark_vel.ARROW
    mark_vel.action = mark_vel.ADD
    mark_vel.scale.x = 0.5 * sqrt(Ux ** 2 + Uy ** 2)
    mark_vel.scale.y = 0.2
    mark_vel.scale.z = 0.2
    mark_vel.color.a = 0.5
    mark_vel.color.r = 1.0
    mark_vel.color.g = 1.0
    mark_vel.color.b = 1.0
    mark_vel.pose.position.x = x_n
    mark_vel.pose.position.y = y_n
    mark_vel.pose.position.z = 0.1
    quaternio = quaternion_from_euler(0, 0, atan2(Uy, Ux))
    mark_vel.pose.orientation.x = quaternio[0]
    mark_vel.pose.orientation.y = quaternio[1]
    mark_vel.pose.orientation.z = quaternio[2]
    mark_vel.pose.orientation.w = quaternio[3]

    # Publica os marcadores, que serao lidos pelo rviz
    pub_rviz_ref.publish(mark_ref)
    pub_rviz_pose.publish(mark_vel)

    return

# ----------  ----------  ----------  ----------  ----------

# Rotina callback para a obtencao dos dados do mapa
def callback_map(data):
    global nCell
    global mapData

    #mapData = data.data

    for i in range(1, nCell + 1, 1):
        for j in range(1, nCell + 1, 1):
            # print data.data[10]
            mapData[i - 1][j - 1] = data.data[(i - 1) * nCell + (j - 1)]
    #mapData = np.flipud(mapData)

    # OBS: cada linha da matriz eh uma linha do grid - as primeiras linhas da matriz sao as linhas de acima do grid
    # OBS: cada coluna da matriz eh uma coluna do grid - as primeiras colunas da matriz sao as colunas da esquerda

    return




# ----------  ----------  ----------  ----------  ----------

# Rotina callback para a obtencao dos dados do mapa
#def callback_map2(data):
    #global nCell
    #global mapData

    #mapData = data.data

    #for i in range(1, nCell + 1, 1):
    #    for j in range(1, nCell + 1, 1):
            # print data.data[10]
    #        mapData[i - 1][j - 1] = data.data[(i - 1) * nCell + (j - 1)]
    #mapData = np.flipud(mapData)

    # OBS: cada linha da matriz eh uma linha do grid - as primeiras linhas da matriz sao as linhas de acima do grid
    # OBS: cada coluna da matriz eh uma coluna do grid - as primeiras colunas da matriz sao as colunas da esquerda

    #return

# ----------  ----------  ----------  ----------  ----------


def isStateValid(state):
    global mapData
    #print(round(state.getX()))
    #print(round(state.getY()))
    #Coo_X = round(state.getX())
    #Coo_y = round(state.getY())
    Coo_X = int(state.getX())
    Coo_Y = int(state.getY())

    #if bool(mapData[Coo_X][Coo_y]==0):
        #print(Coo_X)
        #print(Coo_y)
        #print(bool(mapData[Coo_X][Coo_y]==0))

    #return state.getY() < 5
    return (bool(mapData[Coo_Y][Coo_X]==0))
    #return (bool(mapData[Coo_X][Coo_y] < 0.1))



# ----------  ----------  ----------  ----------  ----------

def planTheHardWay(x_n, y_n, x_goal, y_goal, waypoints_x, waypoints_y):
    global nCell
    # create an SE2 state space
    space = ob.SE2StateSpace()
    # set lower and upper bounds
    bounds = ob.RealVectorBounds(2)
    bounds.setLow(2)
    bounds.setHigh(nCell-3)
    space.setBounds(bounds)


    # construct an instance of space information from this state space
    si = ob.SpaceInformation(space)
    # set state validity checking for this space
    si.setStateValidityChecker(ob.StateValidityCheckerFn(isStateValid))


    # create a random start state
    start = ob.State(space)
    #start.random()
    start().setX(x_n)
    start().setY(y_n)
    # create a random goal state
    goal = ob.State(space)
    #goal.random()
    goal().setX(x_goal)
    goal().setY(y_goal)


    # create a problem instance
    pdef = ob.ProblemDefinition(si)
    # set the start and goal states
    pdef.setStartAndGoalStates(start, goal)


    # create a planner for the defined space
    planner = og.RRTConnect(si)
    #planner = og.RRTstar(si)
    # set the problem we are trying to solve for the planner
    planner.setProblemDefinition(pdef)
    planner.setRange(10)
    # perform setup steps for the planner
    planner.setup()
    # print the settings for this space
    #print(si.settings())
    # print the problem settings
    #print(pdef)
    # attempt to solve the problem within one second of planning time
    solved = planner.solve(1.0)

    if solved:
        # get the goal representation from the problem definition (not the same as the goal state)
        # and inquire about the found path
        #--------------------------------------------------------------
        #path = pdef.getSolutionPath()
        #ps = og.PathSimplifier(path.getSpaceInformation())
        ## path = og.PathSimplifier(path.getSpaceInformation())

        ## path = path.smoothBSpline(path)
        ## path.interpolate()
        #print("Found solution:\n%s" % ps)
        ## print("Found solution:\n%s" % path)
        ## print("Found solution:\n%s" % path.interpolate())
        #num_waypoints = path.getStateCount()
        #for j in range(0, num_waypoints):
        #    waypoints_x[j] = int(ob.State(space, path.getState(j))[0])
        #    # print('X state')
        #    # print(int(ob.State(space, path.getState(j))[0]))
        #    waypoints_y[j] = int(ob.State(space, path.getState(j))[1])
        #--------------------------------------------------------------

        path = pdef.getSolutionPath()
        print("Found solution:\n%s" % path)
        print("---------------------------")

        ps = og.PathSimplifier(path.getSpaceInformation())
        ps.simplifyMax(path)
        #ps.smoothBSpline(path)
        #path.interpolate()
        print("Found solution:\n%s" % path)
        #print("Found solution:\n%s" % path)
        #print("Found solution:\n%s" % path.interpolate())
        num_waypoints = path.getStateCount()
        for j in range (0, num_waypoints):
            waypoints_x[j] = int(ob.State(space, path.getState(j))[0])
            #print('X state')
            #print(int(ob.State(space, path.getState(j))[0]))
            waypoints_y[j] = int(ob.State(space, path.getState(j))[1])
        #print(waypoints_x)
        #print(waypoints_y)
    else:
        print("No solution found")
        waypoints_x = np.zeros(100)
        waypoints_y = np.zeros(100)
        num_waypoints = 0

    return waypoints_x, waypoints_y, num_waypoints


# ----------  ----------  ----------  ----------  ----------

# Rotina primaria
def potential():
    global x_goal, y_goal, x_n, y_n, x_pf, y_pf, x_0, y_0
    global freq, tarefas, flag_tarefas, cont_vel, velo, nt, er, e, ep,  p, s, gr, od, no, fp
    global pub_rviz_ref, pub_rviz_pose, mapData

    vel = Twist()

    # declaracao do topico para comando de velocidade
    pub_stage = rospy.Publisher("/robot_0/cmd_vel", Twist, queue_size=1)

    # inicializa no principal
    rospy.init_node("potential_node")

    # declaracao do topico onde sera requisitado planejamento
    pub_communicate = rospy.Publisher("/communicate", Twist, queue_size=1)

    # declaracao do topico onde sera lido o mapa
    rospy.Subscriber("/map", OccupancyGrid, callback_map)

    # declaracao do topico onde sera lido o mapa
    #rospy.Subscriber("/map2", OccupancyGrid, callback_map2)

    # declaracao do topico onde sera lido a trajetoria
    rospy.Subscriber("/trajectory", Twist, callback_traject)
    # declaracao do topico onde sera lido o estado do robo
    rospy.Subscriber("/robot_0/base_pose_ground_truth", Odometry, callback_pose)
    # declaracao do topico onde sera lido o estado do 'robo' usado com posicao objetivo
    rospy.Subscriber("/robot_1/base_pose_ground_truth", Odometry, callback_goal)
    # declaracao do topico onde sera lido o estado do 'robo' usado com posicao objetivo
    rospy.Subscriber("/robot_2/base_pose_ground_truth", Odometry, callback_pf)
    # declaracao do topico para ler o sensor do robo
    rospy.Subscriber("/robot_0/base_scan", LaserScan, callback_laser)

    # Inicializa os nos para enviar os marcadores para o rviz
    pub_rviz_ref = rospy.Publisher("/visualization_marker_ref", Marker,
                                   queue_size=1)  # rviz marcador de velocidade de referencia
    pub_rviz_pose = rospy.Publisher("/visualization_marker_pose", Marker,
                                    queue_size=1)  # rviz marcador de velocidade do robo


    rate = rospy.Rate(freq)

    br = tf.TransformBroadcaster()
    br.sendTransform((0, 0, 0), (0, 0, 0, 1), rospy.Time.now(), "/map", "world")
    sleep(0.2)

    #np.set_printoptions(threshold='nan')
    #print('map')
    #print(len(mapData))
    #print(mapData)



    #for w in range(0,99,1):
    #    print(mapData[w,:])

    # Inicializa vector com leituras amostradas
    waypoints_x = np.zeros((100,1))
    waypoints_y = np.zeros((100,1))
    num_waypoints = 0

    #x_n = 1
    #y_n = 1
    #x_goal = 100
    #y_goal = 1

    #[waypoints_x, waypoints_y, num_waypoints] = planTheHardWay(x_n, y_n, x_goal, y_goal, waypoints_x, waypoints_y)

    #print('waypoints_x')
    #print(waypoints_x)
    #print('waypoints_y')
    #print(waypoints_y)
    #print('Cont Waypoints')
    #print(num_waypoints)


    ii = 0
    time = 0

    #np.set_printoptions(threshold='nan')
    #print('map')
    #print(mapData)




    #print('Waiting wake-up sign from Matlab')
    #msg1 = rospy.wait_for_message('/trajectory', Twist, timeout=None)
    #print(msg1)



    flag = 1
    while not rospy.is_shutdown():

        #SERA COMENTADO OS EVENTOS CONTROLAVEIS

        if euc_dist(x_n,y_n,x_pf,y_pf) <= 5.2 and flag == 1:
            er = 1
            od = 1
            no = 0
            e = 0
            p = 0
            ep = 0
            fp = 1
        elif flag == 1:
            er = 0
            no = 1
            od = 0
            e = 1
            p = 0
            ep = 0
            fp = 1
        else:
            er = 0
            od = 1
            no = 0
            e = 0
            p = 0
            ep = 1
            fp = 0

        if er == 1:
            print("PR")
            [fatt_x, fatt_y] = attration(tarefas[0], tarefas[1])

            [frep_x, frep_y] = repulsion()

            # Soma as forcas para aplicar ao veiculo
            Ux = fatt_x + frep_x
            Uy = fatt_y + frep_y

            # Aplica o feedback linearization
            [V_forward, w_z] = feedback_linearization(Ux, Uy)

            if abs(V_forward) > Usat:
                V_forward = Usat
            elif V_forward < Umin:
                V_forward = Umin

            velo = velo + V_forward
            cont_vel += 1
            if cont_vel == 20:
                cont_vel = 0
                if velo == 0:
                    print("Local minimum:" + str(velo))
                    er = 0
                    ep = 1
                    e = 0
                    fp = 0
                    flag = 0
                else:
                    flag = 1
                velo = 0
            # Publica as velocidades
            vel.linear.x = V_forward
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = w_z
            pub_stage.publish(vel)
            # Atualiza uma transformacao rigida entre os sistemas de coordenadas do mapa e do mundo
            # Necessario para o rviz apenas
            br = tf.TransformBroadcaster()
            br.sendTransform((0, 0, 0), (0, 0, 0, 1), rospy.Time.now(), "/map", "world")
            # Chama a funcao que envia os marcadores para o rviz
            send_marker_to_rviz(tarefas[0], tarefas[1], Ux, Uy)
            rate.sleep()
        elif e == 1:
            print("PD")
            #print("------------------------------------")
            #print("Plan hard way")
            #planTheHardWay()

            if euc_dist(x_n,y_n,tarefas[0],tarefas[1]) > 0.1:
                print('Request sent')
                vel.linear.x = 0
                vel.linear.y = 1
                vel.linear.z = 0
                vel.angular.x = tarefas[0]
                vel.angular.y = tarefas[1]
                pub_communicate.publish(vel)

                # Publica as velocidades
                vel.linear.x = 0
                vel.linear.y = 0
                vel.linear.z = 0
                vel.angular.x = 0
                vel.angular.y = 0
                vel.angular.z = 0
                Ux = 0
                Uy = 0
                pub_stage.publish(vel)
                # Atualiza uma transformacao rigida entre os sistemas de coordenadas do mapa e do mundo
                # Necessario para o rviz apenas
                br = tf.TransformBroadcaster()
                br.sendTransform((0, 0, 0), (0, 0, 0, 1), rospy.Time.now(), "/map", "world")
                # Chama a funcao que envia os marcadores para o rviz
                send_marker_to_rviz(tarefas[0], tarefas[1], Ux, Uy)
                rate.sleep()

                sleep(1)
                print('position goal')
                print(tarefas[0])
                print(tarefas[1])

                # Inicializa vector com leituras amostradas
                waypoints_x = np.zeros((300, 1))
                waypoints_y = np.zeros((300, 1))
                num_waypoints = 0

                #np.set_printoptions(threshold='nan')
                #print('map')
                #print(len(mapData))
                #print(mapData)

                [waypoints_x, waypoints_y, num_waypoints] = planTheHardWay(x_n, y_n, tarefas[0], tarefas[1], waypoints_x,
                                                                           waypoints_y)


                #while True:
                #    msg2 = rospy.wait_for_message('/trajectory', Twist, timeout=None)
                #    aux = msg2.linear.z
                #    if msg2.angular.x <> -1:
                #        j = msg2.linear.z
                #        num_waypoints = j - 1
                #        #print('Dentro do IF PORQUE nao e -1')
                #        #print('N: ' + str(num_waypoints))
                #        while j >= 1:
                #            #print("Entra no while para ler ate acabarem mensagens")
                #            if aux == j:
                #                print('Le mensagem ' + str(j))
                #                print(msg2)
                #                waypoints_x[num_waypoints - j] = msg2.linear.x
                #                waypoints_y[num_waypoints - j] = msg2.linear.y
                #                print('Posicao :' + str(num_waypoints-j))
                #                j -= 1
                #            msg2 = rospy.wait_for_message('/trajectory', Twist, timeout=None)
                #            aux = msg2.linear.z
                #        j = 0
                #        break


                #print('Saiu do LOOP')
                #print(waypoints_x)
                #print(waypoints_y)

                j = 0
                #print('j: ' + str(j) + ', num_waypoints: ' + str(num_waypoints))
                if j < num_waypoints:
                    p = 1
                    e = 0
                    print(j)
                while j < num_waypoints:
                    if euc_dist(x_n,y_n,x_pf,y_pf) <= 5:
                        p = 0
                        break
                    if euc_dist(x_n,y_n,waypoints_x[j],waypoints_y[j]) < 0.09:
                        j = j+1
                        print('J: ' + str(j))
                        print('Poin verification')
                        print(waypoints_x[j],waypoints_y[j])
                        #print(mapData[int(waypoints_y[j])][int(waypoints_x[j])])
                        #np.set_printoptions(threshold='nan')
                        #print(mapData[int(waypoints_y[j]),:])
                        #print(bool(mapData[int(waypoints_y[j])][int(waypoints_x[j])] == 0))
                        #print('-------------------------------')
                        #print(mapData)

                    [Ux, Uy] = trajectory_controller(waypoints_x[j], waypoints_y[j], 0, 0)
                    # Aplica o feedback linearization
                    [V_forward, w_z] = feedback_linearization(Ux, Uy)
                    #print('erro: ' + str(euc_dist(x_n,y_n,waypoints_x[j],waypoints_y[j])))
                    if V_forward > Usat:
                        V_forward = Usat
                    elif V_forward < Umin:
                        V_forward = Umin

                    # Publica as velocidades
                    vel.linear.x = V_forward
                    vel.linear.y = 0
                    vel.linear.z = 0
                    vel.angular.x = 0
                    vel.angular.y = 0
                    vel.angular.z = w_z
                    pub_stage.publish(vel)
                    # Atualiza uma transformacao rigida entre os sistemas de coordenadas do mapa e do mundo
                    # Necessario para o rviz apenas
                    br = tf.TransformBroadcaster()
                    br.sendTransform((0, 0, 0), (0, 0, 0, 1), rospy.Time.now(), "/map", "world")
                    # Chama a funcao que envia os marcadores para o rviz
                    send_marker_to_rviz(waypoints_x[j], waypoints_y[j], Ux, Uy)
                    rate.sleep()
            else:
                vel.linear.x = 0
                vel.linear.y = 0
                vel.linear.z = 0
                vel.angular.x = 0
                vel.angular.y = 0
                vel.angular.z = 0
                Ux = 0
                Uy = 0
                pub_stage.publish(vel)
                # Atualiza uma transformacao rigida entre os sistemas de coordenadas do mapa e do mundo
                # Necessario para o rviz apenas
                br = tf.TransformBroadcaster()
                br.sendTransform((0, 0, 0), (0, 0, 0, 1), rospy.Time.now(), "/map", "world")
                # Chama a funcao que envia os marcadores para o rviz
                send_marker_to_rviz(tarefas[0], tarefas[1], Ux, Uy)
                rate.sleep()
        elif ep == 1:
            print("PA")
            while time < 10:
                ii = ii + 1
                time = ii / float(freq)
                fp = 0
                [fatt_x, fatt_y] = attration(x_0, y_0)

                [frep_x, frep_y] = repulsion()

                # Soma as forcas para aplicar ao veiculo
                Ux = fatt_x + frep_x
                Uy = fatt_y + frep_y

                # Aplica o feedback linearization
                [V_forward, w_z] = feedback_linearization(Ux, Uy)

                if abs(V_forward) > Usat:
                    V_forward = Usat
                elif V_forward < Umin:
                    V_forward = Umin

                # Publica as velocidades
                vel.linear.x = V_forward
                vel.linear.y = 0
                vel.linear.z = 0
                vel.angular.x = 0
                vel.angular.y = 0
                vel.angular.z = w_z
                pub_stage.publish(vel)
                # Atualiza uma transformacao rigida entre os sistemas de coordenadas do mapa e do mundo
                # Necessario para o rviz apenas
                br = tf.TransformBroadcaster()
                br.sendTransform((0, 0, 0), (0, 0, 0, 1), rospy.Time.now(), "/map", "world")
                # Chama a funcao que envia os marcadores para o rviz
                send_marker_to_rviz(x_0, y_0, Ux, Uy)
                rate.sleep()

            ii = 0
            time = 0
            fp = 1
            ep = 0
            flag = 1
            # Publica as velocidades
            vel.linear.x = 0
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = 0
            Ux = 0
            Uy = 0
            pub_stage.publish(vel)
            # Atualiza uma transformacao rigida entre os sistemas de coordenadas do mapa e do mundo
            # Necessario para o rviz apenas
            br = tf.TransformBroadcaster()
            br.sendTransform((0, 0, 0), (0, 0, 0, 1), rospy.Time.now(), "/map", "world")
            # Chama a funcao que envia os marcadores para o rviz
            send_marker_to_rviz(x_goal, y_goal, Ux, Uy)
            rate.sleep()




# ---------- !! ---------- !! ---------- !! ---------- !! ----------

# Funcao inicial
if __name__ == '__main__':

    try:
        potential()
    except rospy.ROSInterruptException:
        pass


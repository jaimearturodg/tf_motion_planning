#!/usr/bin/env python
import rospy
import smach
import smach_ros
import numpy as np
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from math import sqrt, atan2, cos, sin, pi, ceil, floor
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from time import sleep
from visualization_msgs.msg import Marker
from sensor_msgs.msg import LaserScan
import tf
import sys


# Frequencia de simulacao no stage
global freq
freq = 20.0  # Hz


# Inclinacao da M-line
global theta_ml
theta_ml = 0

# Inicializacao dos estdos do robo
global x_n, y_n, theta_n
x_n = 0.1
y_n = 0.2
theta_n = 0.001

# Inicializao das variaveis do alvo
global x_goal, y_goal
x_goal = 0
y_goal = 0

#scan
global samples
samples = 270

#Range maximo do sensor
global d_s
d_s = 5

# Velocidade de saturacao
global Usat, Umin
Usat = 5
Umin = 0

# Relativo ao feedback linearization
global d
d = 0.40

# Ganho dos controladores
global Kp1, Kp2
Kp1 = 1.5
Kp2 = 1.5

# Flag para deteccao de obstaculos
global obstacle, d_follow, d_reach, circula, aux_x, aux_y, direita, esquerda, calcula_d
obstacle = 0
circula = 1
d_follow = 0
d_reach = 0
direita = 0
esquerda = 0
calcula_d = 0


# Variaveis de referencia para desvio do obstaculo
global x_t, y_t
x_t = 0
y_t = 0


# Frequencia de simulacao no stage
global freq
freq = 20.0  # Hz


# Inclinacao da M-line
global theta_ml
theta_ml = 0

# Inicializacao dos estdos do robo
global x_n, y_n, theta_n
x_n = 0.1
y_n = 0.2
theta_n = 0.001

# Inicializao das variaveis do alvo
global x_goal, y_goal
x_goal = 0
y_goal = 0

#scan
global samples
samples = 270

#Range maximo do sensor
global d_s
d_s = 5

# Velocidade de saturacao
global Usat, Umin
Usat = 5
Umin = 0

# Relativo ao feedback linearization
global d
d = 0.40

# Ganho dos controladores
global Kp1, Kp2
Kp1 = 1.5
Kp2 = 1.5

# Flag para deteccao de obstaculos
global obstacle, d_follow, d_reach, circula, aux_x, aux_y, direita, esquerda, calcula_d
obstacle = 0
circula = 1
d_follow = 0
d_reach = 0
direita = 0
esquerda = 0
calcula_d = 0


# Variaveis de referencia para desvio do obstaculo
global x_t, y_t
x_t = 0
y_t = 0

# ----------  ----------  ----------  ----------  ----------

# Rotina para calcular inclinacao da m-line
def inc_mline(xr, yr, xn, yn):
    x = xn - xr
    y = yn - yr
    theta = atan2(y, x)
    return theta
# ----------  ----------  ----------  ----------  ----------

# Rotina para calcular distancia euclidiana
def euc_dist(x1, y1, x2, y2):
    return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2))
# ----------  ----------  ----------  ----------  ----------

# Rotina para rotacao do referencial do robo para o referencial inercial
def rotacao(xr, yr, theta):
    xi = cos(theta) * xr - sin(theta) * yr
    yi = sin(theta) * xr + cos(theta) * yr
    return xi, yi
# ----------  ----------  ----------  ----------  ----------

# Euristica para calculo da menor distancia
def euristica(disc_x, disc_y, disc_ind):
    global x_n, y_n, x_goal, y_goal
    i = 0
    smaller_dist = 1000
    smaller_ind = -1
    while disc_ind[i] >= 0:
        d = euc_dist(x_n, y_n, disc_x[i], disc_y[i]) + euc_dist(disc_x[i], disc_y[i], x_goal, y_goal)
        if d <= smaller_dist:
            smaller_dist = d
            smaller_ind = i
        i += 1

    return smaller_ind
# ----------  ----------  ----------  ----------  ----------

# Rotina callback para a obtencao da pose do robo
def callback_pose(data):
    global x_n, y_n, theta_n

    x_n = data.pose.pose.position.x  # posicao 'x' do robo no mundo
    y_n = data.pose.pose.position.y  # posicao 'y' do robo no mundo
    x_q = data.pose.pose.orientation.x
    y_q = data.pose.pose.orientation.y
    z_q = data.pose.pose.orientation.z
    w_q = data.pose.pose.orientation.w
    euler = euler_from_quaternion([x_q, y_q, z_q, w_q])
    theta_n = euler[2]  # orientacao 'theta' do robo no mundo

    return

# ----------  ----------  ----------  ----------  ----------

# Rotina callback para a obtencao da pose do robo que representa o goal
def callback_goal(goal):
    global x_goal, y_goal, theta_goal

    x_goal = goal.pose.pose.position.x  # posicao 'x'
    y_goal = goal.pose.pose.position.y  # posicao 'y'
    x_q2 = goal.pose.pose.orientation.x
    y_q2 = goal.pose.pose.orientation.y
    z_q2 = goal.pose.pose.orientation.z
    w_q2 = goal.pose.pose.orientation.w
    euler2 = euler_from_quaternion([x_q2, y_q2, z_q2, w_q2])
    theta_goal = euler2[2]  # orientacao 'theta'

    return


# ----------  ----------  ----------  ----------  ----------

# Rotina callback para a obtencao da informacao do laser
def callback_laser(scan):
    global theta_n, samples, x_n, y_n, theta_n, obstacle, x_t, y_t, x_goal, y_goal
    global d_reach, d_follow, circula, aux_x, aux_y, esquerda, direita, calcula_d

    cont = 1
    k = 0
    # Inicializa vector com leituras amostradas
    filt_ranges = 5*np.ones(55)

    # Amostra feixes por um fator de 5 unidades
    for i in range(0, samples):
        cont -= 1
        if cont == 0:
            filt_ranges[k] = scan.ranges[i]
            k += 1
            cont = 5

    disconts_ind = -1*np.ones(55)
    disconts_val = -1*np.ones(55)
    disconts_x = np.zeros(55)
    disconts_y = np.zeros(55)
    disconts_side = np.ones((55), dtype=bool)
    k = 0
    i = 0
    aux = True
    while i < 55:
        if filt_ranges[i] < 5:
            # Descontinuidade j em i
            disconts_ind[k] = i
            disconts_val[k] = filt_ranges[i]
            disconts_side[k] = aux
            aux = not(aux)
            k += 1
            i += 1
            while i < 55:
                #print filt_ranges[i]
                if filt_ranges[i] == 5:
                    # Descontinuidade j + 1 em i-1
                    disconts_ind[k] = i - 1
                    disconts_val[k] = filt_ranges[i - 1]
                    disconts_side[k] = aux
                    aux = not(aux)
                    k += 1
                    break
                elif i == 54:
                    # Descontinuidade fim do range do sensor
                    disconts_ind[k] = i
                    disconts_val[k] = filt_ranges[i]
                    disconts_side[k] = aux
                    break
                else:
                    i += 1
        i +=1
    j = 0
    while j<k:

        angle = 5*disconts_ind[j] #Angulo indicado pelo sensor
        angle = (angle - 135)*pi/180 # Angulo no referencial do robo [rad]
        xr = disconts_val[j]*cos(angle)
        yr = disconts_val[j]*sin(angle)
        [xi, yi] = rotacao(xr, yr, theta_n)
        # Posicao da discontinuidade no referencial inercial
        disconts_x[j] = x_n + xi
        disconts_y[j] = y_n + yi
        j += 1

    if k > 0:
        [xr, yr] = rotacao((x_goal-x_n), (y_goal-y_n), -theta_n)
        ang_mline = inc_mline(0, 0, xr, yr)*180/pi
        if ang_mline < -135 or ang_mline > 135:
            #Direcao da m-line esta fora do range do sensor
            obstacle = 0
            circula = 0
            esquerda = 0
            direita = 0
            calcula_d = 0
        else:
            ind_mline = (ang_mline+135)/5
            ind_floor = int(floor(ind_mline)) - 1
            ind_ceil = int(ceil(ind_mline)) + 1
            if ind_mline < 2:
                ind_ceil = 2
                ind_floor = 0
            elif ind_mline > 52:
                ind_floor = 52
                ind_ceil = 54

            if filt_ranges[ind_ceil] == 5 and filt_ranges[ind_floor] == 5:
                #Existe caminho livre para o objetivo
                obstacle = 0
                circula = 0
                esquerda = 0
                direita = 0
                calcula_d = 0
            else:
                #Nao existe caminho livre para o objetivo
                obstacle = 1
                ind = euristica(disconts_x, disconts_y, disconts_ind)
                if calcula_d == 1 and (aux_x != x_goal or aux_y != y_goal):
                    calcula_d = 0
                if calcula_d == 0:
                    d_follow = euc_dist(x_goal, y_goal, disconts_x[ind], disconts_y[ind])
                    aux_x = x_goal
                    aux_y = y_goal
                    calcula_d = 1

                if circula == 0:
                    d_reach = euc_dist(x_goal, y_goal, disconts_x[ind], disconts_y[ind])

                if d_reach > d_follow or circula == 1:
                    # Circula objeto
                    circula = 1
                    if direita == 1 and esquerda == 0:
                        smaller = filt_ranges[54]
                        i = 54
                        index = i
                        while i >= 54 - 180 / 5:
                            if filt_ranges[i] < smaller:
                                smaller = filt_ranges[i]
                                index = i
                            i -= 1
                        angle = 5 * (index - 18)
                        angle = (angle - 135) * pi / 180

                    elif esquerda == 1 and direita == 0:
                        smaller = disconts_val[0]
                        i = 0
                        index = i
                        while i <= 54 - 90 / 5:
                            if filt_ranges[i] < smaller:
                                smaller = filt_ranges[i]
                                index = i
                            i += 1

                        angle = 5 * (index + 18)
                        angle = (angle - 135) * pi / 180

                    if direita == 1 and esquerda == 0:
                        if disconts_side[ind] == True:
                            d_reach = euc_dist(x_goal, y_goal, disconts_x[ind], disconts_y[ind])
                        else:
                            d_reach = euc_dist(x_goal, y_goal, disconts_x[ind-1], disconts_y[ind-1])
                    elif direita == 0 and esquerda ==1:
                        if disconts_side[ind] == False:
                            d_reach = euc_dist(x_goal, y_goal, disconts_x[ind], disconts_y[ind])
                        else:
                            d_reach = euc_dist(x_goal, y_goal, disconts_x[ind+1], disconts_y[ind+1])
                    if d_reach > d_follow - 5:
                        xr = 0.5 * cos(angle)
                        yr = 0.5 * sin(angle)
                        [xi, yi] = rotacao(xr, yr, theta_n)
                        x_t = xi + x_n
                        y_t = yi + y_n
                    else:
                        circula = 0

                if circula == 0:
                    # Segue descontinuidade

                    [xr, yr] = rotacao((disconts_x[ind] - x_n), (disconts_y[ind] - y_n), -theta_n)
                    xr = xr + 0.5
                    if disconts_side[ind] == True:
                        yr = yr - 1
                        direita = 1
                        esquerda = 0
                    else:
                        yr = yr + 1
                        direita = 0
                        esquerda = 1
                    [xi, yi] = rotacao(xr, yr, theta_n)
                    x_t = xi + x_n
                    y_t = yi + y_n

                    d_reach = euc_dist(x_goal, y_goal, disconts_x[ind], disconts_y[ind])
    else:
        obstacle = 0
        circula = 0
        esquerda = 0
        direita = 0
        calcula_d =0
    return

# ----------  ----------  ----------  ----------  ----------

# Rotina para a geracao da trajetoria de referencia
def refference_trajectory(time):
    global theta_ml, d_s, y_goal, x_goal, y_n, x_n, obstacle, x_t, y_t

    if obstacle == 0:
        d_goal = euc_dist(x_goal, y_goal, x_n, y_n)

        if d_s >= d_goal:
            x_ref = x_goal
            y_ref = y_goal
        else:
            theta_ml = inc_mline(x_n,y_n,x_goal,y_goal)
            x_ref = d_s*cos(theta_ml)+x_n
            y_ref = d_s*sin(theta_ml)+y_n
    else:
        x_ref = x_t
        y_ref = y_t

    Vx_ref = 0
    Vy_ref = 0


    return x_ref, y_ref, Vx_ref, Vy_ref

# ----------  ----------  ----------  ----------  ----------

# Rotina para a geracao da entrada de controle
def trajectory_controller(x_ref, y_ref, Vx_ref, Vy_ref):
    global x_n, y_n
    global Kp1, Kp2

    Ux = Vx_ref + Kp1*(x_ref - x_n)
    Uy = Vy_ref + Kp2*(y_ref - y_n)

    return Ux, Uy


# ----------  ----------  ----------  ----------  ----------

# Rotina feedback linearization
def feedback_linearization(Ux, Uy):
    global x_n, y_n, theta_n
    global d

    VX = cos(theta_n)*Ux + sin(theta_n)*Uy
    WZ = ((-sin(theta_n))/d)*Ux + ((cos(theta_n))/d)*Uy

    return VX, WZ

# Rotina primaria
def tangentbug():
    global Usat, Umin, obstacle
    global freq
    vel = Twist()

    i = 0

    # declaracao do topico para comando de velocidade
    pub_stage = rospy.Publisher("/robot_0/cmd_vel", Twist, queue_size=1)
    # inicializa no principal
    rospy.init_node("potential_node")
    # declaracao do topico onde sera lido o estado do robo
    rospy.Subscriber("/robot_0/base_pose_ground_truth", Odometry, callback_pose)
    # declaracao do topico onde sera lido o estado do 'robo' usado com posicao objetivo
    rospy.Subscriber("/robot_1/base_pose_ground_truth", Odometry, callback_goal)
    # declaracao do topico para ler o sensor do robo
    rospy.Subscriber("/robot_0/base_scan", LaserScan, callback_laser)
    #Define uma variavel que controlar[a a frequencia de execucao deste no
    rate = rospy.Rate(freq)

    sleep(0.2)

    # O programa do no consiste no codigo dentro deste while
    while not rospy.is_shutdown(): #"Enquanto o programa nao ser assassinado"

        # Incrementa o tempo
        i = i + 1
        time = i / float(freq)

        [x_ref, y_ref, Vx_ref, Vy_ref] = refference_trajectory(time)

        [Ux, Uy] = trajectory_controller(x_ref, y_ref, Vx_ref, Vy_ref)

        # Aplica o feedback linearization
        [V_forward, w_z] = feedback_linearization(Ux, Uy)

        if abs(V_forward) > Usat:
            V_forward = Usat
        elif V_forward < Umin:
            V_forward = Umin

        # Publica as velocidades
        vel.linear.x = V_forward
        vel.angular.z = w_z
        pub_stage.publish(vel)

        #Espera por um tempo de forma a manter a frequencia desejada
        rate.sleep()




############################################################################################################

# define state Foo
class Foo(smach.State):
    def __init__(self):
        smach.State.__init__(self,
                             outcomes=['outcome1', 'outcome2'],
                             input_keys=['foo_counter_in'],
                             output_keys=['foo_counter_out'])

    def execute(self, userdata):
        rospy.loginfo('Executing state FOO')
        if userdata.foo_counter_in < 3:
            userdata.foo_counter_out = userdata.foo_counter_in + 1
            return 'outcome1'
        else:
            return 'outcome1'
            #return 'outcome2'


# define state Bar
class Bar(smach.State):
    def __init__(self):
        smach.State.__init__(self,
                             outcomes=['outcome1'],
                             input_keys=['bar_counter_in'])

    def execute(self, userdata):
        rospy.loginfo('Executing state BAR')
        rospy.loginfo('Counter = %f' % userdata.bar_counter_in)
        return 'outcome1'


def main():
    rospy.init_node('smach_example_state_machine')

    # Create a SMACH state machine
    sm = smach.StateMachine(outcomes=['outcome4'])
    sm.userdata.sm_counter = 0

    # Open the container
    with sm:
        # Add states to the container
        smach.StateMachine.add('FOO', Foo(),
                               transitions={'outcome1': 'BAR',
                                            'outcome2': 'outcome4'},
                               remapping={'foo_counter_in': 'sm_counter',
                                          'foo_counter_out': 'sm_counter'})
        smach.StateMachine.add('BAR', Bar(),
                               transitions={'outcome1': 'FOO'},
                               remapping={'bar_counter_in': 'sm_counter'})

    # Execute SMACH plan
    outcome = sm.execute()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
